@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/medicines') }}">Medicine</a> :
@endsection
@section("contentheader_description", $medicine->$view_col)
@section("section", "Medicines")
@section("section_url", url(config('laraadmin.adminRoute') . '/medicines'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Medicines Edit : ".$medicine->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($medicine, ['route' => [config('laraadmin.adminRoute') . '.medicines.update', $medicine->id ], 'method'=>'PUT', 'id' => 'medicine-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'name')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/medicines') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#medicine-edit-form").validate({
		
	});
});
</script>
@endpush
