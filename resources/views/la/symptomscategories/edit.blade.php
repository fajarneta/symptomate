@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/symptomscategories') }}">SymptomsCategory</a> :
@endsection
@section("contentheader_description", $symptomscategory->$view_col)
@section("section", "SymptomsCategories")
@section("section_url", url(config('laraadmin.adminRoute') . '/symptomscategories'))
@section("sub_section", "Edit")

@section("htmlheader_title", "SymptomsCategories Edit : ".$symptomscategory->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($symptomscategory, ['route' => [config('laraadmin.adminRoute') . '.symptomscategories.update', $symptomscategory->id ], 'method'=>'PUT', 'id' => 'symptomscategory-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'category')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/symptomscategories') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#symptomscategory-edit-form").validate({
		
	});
});
</script>
@endpush
